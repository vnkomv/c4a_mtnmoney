<?php

function postCurlMtn($lien,$post){

	$ch=curl_init();
	curl_setopt($ch,CURLOPT_URL,$lien);
	curl_setopt ($ch, CURLOPT_POST, 1);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_POSTFIELDS, http_build_query($post, NULL, '&'));
	curl_exec($ch);
	$return = curl_multi_getcontent ($ch);
	curl_close($ch);
	return $return;
}


function mtnmoney_config() {
    $configarray = array(
	    "FriendlyName" => array("Type" => "System", "Value"=>"MTN MONEY"),
        
	    "instructions" => array(
                                  "FriendlyName" => "Payment Instructions",
                                  "Type" => "textarea",
                                  "Rows" => "5",
                                  "Description" => "Important à réaliser pour effectuer le paiement...",
                                    ),
            "MtnMoneyMarchandUsername" => array("FriendlyName"=>"Nom d'utilisateur du marchand MtnMoney",
                                                "Type"=>"text",
                                                "Value"=>"",
                                               ),
                                               
            "MtnMoneyMarchandPassword" => array("FriendlyName" => "Mot de passe du marchand MtnMoney",
                                                "Type" => "text",
                                                "Value" => "",
                                          ),
	    "MtnMoneyMarchandKey"=>array("FriendlyName"=>"Identifiant du marchand MtnMoney",
                                         "Type"=>"text",
                                         "Value"=>""),
            
            "testmode" => array("FriendlyName" => "Test Mode",
                                "Type" => "yesno",
                                "Description" => "Choisissez le mode de fonctionnement du module", 
                            ),
    );
	return $configarray;
}


function mtnmoney_link($params) {

	addToLog_mtnmoney('**************************Init mtnmoney****************************');
	addToLog_mtnmoney("===============>mtnmoney_link - Start");
	addToLog_mtnmoney("Client : ".$params['clientdetails']['lastname']." ".$params['clientdetails']['firstname']);
	addToLog_mtnmoney("email : ".$params['email']);
	# Gateway Specific Variables

	$gatewaytestmode = $params['testmode'];

	# Invoice Variables
	$invoiceid = $params['invoiceid'];
	//$invoiceid=$invoiceid."-".uniqid();
//	$merchantid ="f83c4313d153016fad13bff2952d0f1a50722bb9fa46dadb576e3d999ba79b6b";
	$merchantid =$params["MtnMoneyMarchandKey"];
	$description = $params["description"];
    $amount = $params['amount']; # Format: ##.##
    $currency = $params['currency']; # Currency Code
    $code = 'T-'.date('ymd-hms');
    $sessionid = uniqid($code);
    $logo = "http://www.cloud4africa.net/templates/ace/img/logo.png";

    $postfields = array(
		'merchantid' => $merchantid,
		'sessionid' => $sessionid,
		'purchaseref' => $invoiceid,
		'amount' => intval($amount),
	);
	addToLog_mtnmoney("parametres d'initialisation = ". print_r($postfields, true));
//	addToLog_mtnmoney("Initialisation session mtnmoney...");
    //postCurl('http://test.mtnmoney.lo',$postfields);
   // $token = postCurl('https://mtnmoney.orange.ci/e-commerce_test_gw/init.php',$postfields);
   	addToLog_mtnmoney("...reussite. token mtnmoney = ".$token);
   	addToLog_mtnmoney("GATEWAYTEST = ".$gatewaytestmode);

	# System Variables
	$companyname = $params['companyname'];
	$systemurl = $params['systemurl'];
	$currency = $params['currency'];

	$firstname = $params['clientdetails']['firstname'];
	$lastname = $params['clientdetails']['lastname'];
	$email = $params['clientdetails']['email'];
	$address1 = $params['clientdetails']['address1'];
	$address2 = $params['clientdetails']['address2'];
	$city = $params['clientdetails']['city'];
	$state = $params['clientdetails']['state'];



		$gateway_redirection="http://www.cloud4africa.net/mtnmoneyarea/MtnPaymentOnline.action";

	# Enter your code submit to the gateway...https://mtnmoney.orange.ci/e-commerce_test_gw/
	addToLog_mtnmoney("Redirection vers la plateforme mtnmoney...");
	$code = '<form action="'.$gateway_redirection.'" method="POST">
<input type="hidden" name="testmode" value="'.$gatewaytestmode.'" />
<input type="hidden" name="token" value="'.$token.'" />
<input type="hidden" name="merchantid" value="'.$merchantid.'" />
<input type="hidden" name="description" value="'.$description.'" />
<input type="hidden" name="invoiceid" value="'.$invoiceid.'" />,
<input type="hidden" name="purchaseref" value="'.$invoiceid.'" />
<input type="hidden" name="logo" value="'.$logo.'" />
<input type="hidden" name="amount" value="'.$amount.'" />
<input type="hidden" name="firstname" value="'.$firstname.'" />
<input type="hidden" name="lastname" value="'.$lastname.'" />
<input type="hidden" name="email" value="'.$email.'" />
<input type="hidden" name="adress1" value="'.$address1.'" />
<input type="hidden" name="adress2" value="'.$address2.'" />
<input type="hidden" name="city" value="'.$city.'" />
<button type="submit" class="mtnmoney" value="mtnmoney">
	Payer la commande

</button>
</form>';
	addToLog_mtnmoney("===============>mtnmoney_link - End\n");
	return $code;
}

function addToLog_mtnmoney($message){
	$current_date=date("d/m/Y H:i:s");
	file_put_contents(dirname(__FILE__)."/log_mtnmoney/mtnmoney.log","[".$current_date."] ".$message."\n",FILE_APPEND);
}
function test(){

}
?>