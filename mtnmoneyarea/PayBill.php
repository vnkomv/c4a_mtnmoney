 <?php

define("CLIENTAREA",true);
 //define("FORCESSL",true); // Uncomment to force the page to use https://
 //header("content-type","text/xml");
// header('Content-type: application/xml');
 //require("../init.php");
 # Required File Includes
 //ini_set("display_errors",1);
 require_once("../dbconnect.php");
 //include("../../../includes/PDOConfig.php");
 require_once("../includes/functions.php");
 require_once("../includes/gatewayfunctions.php");
require_once("../includes/invoicefunctions.php");

header('Content-type: application/xml');

 //-------------------------------
 function addToLogMtnPayBill($file,$message){

 	$current_date=date("d/m/Y H:i:s");
 	file_put_contents(dirname(__FILE__)."/logs/$file","[".$current_date."] ".$message."\n",FILE_APPEND);

 }



 //--------------------------------

 function transactAlreadyUsed($transact){

 	$data=mysql_query("select * from mtn_money_transact where transact='$transact'");
 	$nb_lines=mysql_num_rows($data);

 	if($nb_lines>0){
 		return true;
 	}else{
 		return false;
 	}

 }

 //-----------------------

 try{
 	$user="mtnmoney";
 	$pass="6SGDHmXR7AFs";
 	$current_d=date("Y-m-d H:i:s");

 	$invoiceid=mysql_real_escape_string($_REQUEST['Reference']);
 	$invoiceid=strtoupper($invoiceid);
 	$montant=mysql_real_escape_string($_REQUEST['Montant']);
 	$transact=mysql_real_escape_string($_REQUEST['Transact']);

 //	$invoiceid=mysql_real_escape_string($invoiceid);

 	$adminuser="admin";
 	$command="getinvoice";
 	$values=array();
 //	$invoiceid="25";
 	$values["invoiceid"]=$invoiceid;

 	$gatewaymodule = "mtnmoney";
 	//addToLog_callback("===============>OMPAY callback - Start");
 	# Enter your gateway module name here replacing template


 //	addToLog_callback("...retour sur cloud4africa.");
 	$GATEWAY = getGatewayVariables($gatewaymodule);

 //	print_r($GATEWAY);

 //	$info_client=localAPI("getclientsdetails",array("clientid"=>11),$adminuser);
  //print_r($info_client);
 	$info_invoice=localAPI($command,$values,$adminuser);

 //	print_r($info_invoice);

 //	echo intval($info_invoice["total"]);
    $delai_paiement="2014-12-31";
 	$session_id=$info_invoice["userid"];
 	$empty_data='<ArrayOfFacture></ArrayOfFacture>';

 //	$result_xml=$empty_data;
 //	$result_xml='<Paiement><CODE>1</CODE><NUMFACTURE></NUMFACTURE><DATEPAIEMENT>'.date('Y-m-d').'</DATEPAIEMENT><MONTANT>0</MONTANT><COMMENTAIRE>NON AUTORISE</COMMENTAIRE></Paiement>';

 if(($_REQUEST['User']!=$user) || ($_REQUEST['Pass']!=$pass))
 	{

 	$result_xml='<Paiement><CODE>1</CODE><NUMFACTURE></NUMFACTURE><DATEPAIEMENT>'.date('Y-m-d').'</DATEPAIEMENT><MONTANT>0</MONTANT><COMMENTAIRE>NON AUTORISE</COMMENTAIRE></Paiement>';
    addToLogMtnPayBill("paybill.log","MOT DE PASSE INCORRECT" );
 	}elseif(!$GATEWAY["type"]){

 	$result_xml='<?xml version="1.0" encoding="utf-8"?><Paiement><CODE>1</CODE><NUMFACTURE></NUMFACTURE><DATEPAIEMENT>'.date('Y-m-d').'</DATEPAIEMENT><MONTANT>0</MONTANT><COMMENTAIRE>NON AUTORISE</COMMENTAIRE></Paiement>';

 		addToLogMtnPayBill("paybill.log","MODULE MTN MONEY NON ACTIVE POUR LE ".$invoiceid );

	}elseif(transactAlreadyUsed($transact)){

 	/*	$data='<?xml version="1.0" encoding="utf-8"?>';*/
 		$data=	'<Paiement>';
 		$data.=	'<CODE>3</CODE>';
 		$data.=	'<NUMFACTURE></NUMFACTURE>';
 		$data.=	'<DATEPAIEMENT>'.$current_d.'</DATEPAIEMENT>';
 		$data.=	'<MONTANT>0</MONTANT>';
 		$data.=	'<COMMENTAIRE>NUMERO DE TRANSACTION DEJA UTILISE</COMMENTAIRE>';
 		$data.=	'</Paiement>';

 		$result_xml=$data;
		addToLogMtnPayBill("paybill.log","NUMERO DE TRANSACTION DEJA UTILISE POUR LE ".$invoiceid );

 	}elseif($info_invoice["result"]!="success"){

 	/*	$data='<?xml version="1.0" encoding="utf-8"?>';*/
 		$data=	'<Paiement>';
 		$data.=	'<CODE>4</CODE>';
 		$data.=	'<NUMFACTURE></NUMFACTURE>';
 		$data.=	'<DATEPAIEMENT>'.date('Y-m-d').'</DATEPAIEMENT>';
 		$data.=	'<MONTANT>0</MONTANT>';
 		$data.=	'<COMMENTAIRE>AUCUNE FACTURE CORRESPONDANTE</COMMENTAIRE>';
 		$data.=	'</Paiement>';

 		$result_xml=$data;

 		addToLogMtnPayBill("paybill.log","AUCUNE FACTURE CORRESPONDANTE POUR LE ".$invoiceid );



 	}elseif(strtoupper($info_invoice["status"])=="PAID"){


 	/*	$data='<?xml version="1.0" encoding="utf-8"?>';*/
 		$data=	'<Paiement>';
 		$data.=	'<CODE>0</CODE>';
 		$data.=	'<NUMFACTURE>'.$invoiceid.'</NUMFACTURE>';
 		$data.=	'<DATEPAIEMENT>'.$info_invoice["datepaid"].'</DATEPAIEMENT>';
 		$data.=	'<MONTANT>'.intval($info_invoice["total"]).'</MONTANT>';
 		$data.=	'<COMMENTAIRE>PAIEMENT DEJA EFFECTUE</COMMENTAIRE>';
 		$data.=	'</Paiement>';

 		$result_xml=$data;

 		addToLogMtnPayBill("paybill.log","PAIEMENT DEJA EFFECTUE POUR LE ".$invoiceid );

 	} elseif (($info_invoice["invoiceid"]!=$invoiceid)||(intval($info_invoice["total"])!=$montant)){

 	/*	$data='<?xml version="1.0" encoding="utf-8"?>';*/
 		$data=	'<Paiement>';
 		$data.=	'<CODE>2</CODE>';
 		$data.=	'<NUMFACTURE>'.$invoiceid.'</NUMFACTURE>';
 		$data.=	'<DATEPAIEMENT>'.$current_d.'</DATEPAIEMENT>';
 		$data.=	'<MONTANT>0</MONTANT>';
 		$data.=	'<COMMENTAIRE>MONTANT OU REFERENCE NON CORRESPONDANTE</COMMENTAIRE>';
 		$data.=	'</Paiement>';

 		$result_xml=$data;

 		addToLogMtnPayBill("paybill.log","MONTANT OU REFERENCE NON CORRESPONDANTE  POUR LE ".$invoiceid );


 	}else{// c ok

 		addInvoicePayment($invoiceid,$sessionid,$montant,0,$gatewaymodule); # Apply Payment to Invoice: invoiceid, transactionid, amount paid, fees, modulename
 		logTransaction($GATEWAY["name"],$_REQUEST,"Successful"); # Save to Gateway Log: name, data array, status

 		mysql_query("insert into mtn_money_transact (transact) values ('$transact') ");
 		//$values["su
 	/*	$data='<?xml version="1.0" encoding="utf-8"?>';*/
 		$data='<Paiement>';
 		$data.='<CODE>0</CODE>';
 		$data.='<NUMFACTURE>'.$invoiceid.'</NUMFACTURE>';
 		$data.='<DATEPAIEMENT>'.$current_d.'</DATEPAIEMENT>';
 		$data.='<MONTANT>'.$montant.'</MONTANT>';
 		$data.='<COMMENTAIRE>OK</COMMENTAIRE>';
 		$data.='</Paiement>';

 		$result_xml=$data;

 		addToLogMtnPayBill("paybill.log","PAIEMENT EFFECTUE   POUR LE ".$invoiceid." NUMERO DE TRANSACTION: ".$transact." MONTANT:".$montant );

 	}

 }catch(Exception $e){
 	$result_xml='<Paiement><CODE>1</CODE><NUMFACTURE></NUMFACTURE><DATEPAIEMENT>'.date('Y-m-d').'</DATEPAIEMENT><MONTANT>0</MONTANT><COMMENTAIRE>NON AUTORISE</COMMENTAIRE></Paiement>';


 }

echo trim($result_xml);
 /*echo  '<?xml version="1.0" encoding="utf-8"?><Paiement><CODE>1</CODE><NUMFACTURE></NUMFACTURE><DATEPAIEMENT>'.date('Y-m-d').'</DATEPAIEMENT><MONTANT>0</MONTANT><COMMENTAIRE>NON AUTORISE</COMMENTAIRE></Paiement>';*/


 ?>